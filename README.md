# README

### **Requirements**:
* minikube or kubernetes installed
* kubectl installed
* helm installed

## A. Clone of the Repo
```bash
https://gitlab.com/simowalter/local_deployment_calculator_with_helm.git
cd local_deployment_calculator_with_helm
```

## B. Installation and Deployment

1. Installation with Helm
Test
```bash
helm template calculator-web-app > all-in-one.yml
```
Installation
```bash
helm install calculator-web-app --generate-name
```

2. Packaging
```bash
helm package calculator-web-app
```

3. Port forward of the frontend service
```bash
kubectl port-forward -n calculator-web-app svc/service-frontend-calculator 8000:80
```

4. Port forward of the backend service
```bash
kubectl port-forward -n calculator-web-app svc/service-backend-calculator 8001:80
```

Remark: If you run the code on a virtual server with no graphical interface, you can create ssh tunner for forwded port as follows:
```bash
ssh -f username_Instance@IP_Instance -L 8000:localhost:8000 -N
ssh -f username_Instance@IP_Instance -L 8001:localhost:8001 -N
```

5. See the deployed web application with the url
```bash
http://localhost:8080
```
*Walter Simo*
